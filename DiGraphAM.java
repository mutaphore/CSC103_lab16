public class DiGraphAM {
	
	private int [][] adjTable;
	
	public DiGraphAM(int N) {
	
		adjTable = new int[N][N];
	
	}
	
	public void addEdge(int from, int to) {
		
		adjTable[from][to] = 1;
		
	}
	
	public void deleteEdge(int from, int to) {
		
		adjTable[from][to] = 0;
		
	}
	
	public int edgeCount() {
		
		int count = 0;
		
		for(int i=0; i<adjTable.length ; i++) {
			
			for(int j=0; j<adjTable.length ; j++) {
			
				if(adjTable[i][j] == 1)
					count++;
				
			}
			
		}
		
		return count;
		
	}
	
	public int vertexCount() {
		
		return adjTable.length;
		
	}
	
	public void print() {
		
		for(int i=0; i<adjTable.length; i++) {
			
			System.out.print(i + " is connected to: " );
				
			for(int j=0; j<adjTable.length; j++) {
				
				if(adjTable[i][j] == 1) { 
					System.out.print(j + " ");
				}
			}
		
			System.out.println();
		
		}
		
	}
	
	private int[] indegrees() {
		
		int[] answer = new int[adjTable.length];
		
		for(int v=0; v<adjTable.length; v++) {
			
			for(int x=0; x<adjTable.length; x++) {
				
				if(adjTable[v][x] == 1)
					answer[x]++;
				
			}
			
		}
		
		return answer;
		
	}
	
	public int[] topSort() {
		
		int[] temp = indegrees();
		int [] answer = new int[temp.length];
		LQueue<Integer> queue = new LQueue<Integer>();
		int count = 0;
		int v;
		int index = 0;
		
		for(int i=0; i<temp.length; i++) {
			
			if(temp[i] == 0)
				queue.enqueue(i);
			
		}
		
		while(!queue.isEmpty()) {
			
			v=queue.dequeue();
			answer[index] = v;
			count++;
			
			for(int i=0; i<adjTable.length; i++){
				
				if(adjTable[v][i] == 1) {
				
					temp[i]--;
					if(temp[i] == 0)
						queue.enqueue(i);
				
				}
				
			}
			
			index++;
			
		}
		
		if(count != adjTable.length) {
			
			throw new RuntimeException();
			
		}
		
		return answer;
		
	}
	
	
}