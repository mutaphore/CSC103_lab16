import java.util.*;

public class DiGraphTest {
	
	public static void main(String[] args) {
	
		Scanner input = new Scanner(System.in);
		int N;
		char choice = 'z';
		int temp1;
		int temp2;
		int[] temp;
	
		System.out.println("Please enter number of vertices: ");
		N = input.nextInt();
		input.nextLine();
		DiGraphAM matrix = new DiGraphAM(N);
	
	    System.out.println("Choose one of the following operations:");
	    System.out.println("- add edge (enter the letter a)");
	    System.out.println("- delete edge (enter the letter d)");
	    System.out.println("- edge count (enter the letter e)");
	    System.out.println("- vertex count (enter the letter v)");
	    System.out.println("- print (enter the letter p)");
	    System.out.println("- topological sort (enter the letter t)");
	    System.out.println("- quit (enter the letter q)");
	
	    while(choice != 'q')
	    {
	       System.out.println("Enter choice: ");
	       choice = input.nextLine().charAt(0);

	       switch (choice)
	       {
				case 'a':
				System.out.print("Enter source and destination of edge to be added: ");
				temp1 = input.nextInt();
				temp2 = input.nextInt();
				input.nextLine();
				matrix.addEdge(temp1,temp2);
				System.out.print("The edge (" + temp1 + ", " + temp2 + ") has been added\n");
				break;
			
				case 'd':
				System.out.print("Enter source and destination of edge to be deleted: ");
				temp1 = input.nextInt();
				temp2 = input.nextInt();
				input.nextLine();
				matrix.deleteEdge(temp1,temp2);
				System.out.print("The edge (" + temp1 + ", " + temp2 + ") has been deleted\n");
				break;
			
				case 'e':
				System.out.println("The number of edges is " + matrix.edgeCount());
				break;
			
				case 'v':
				System.out.println("The number of vertices is " + matrix.vertexCount());
				break;

				case 'p':
				System.out.println("The content of the adjacency table of the graph is the following: ");
				matrix.print();
				break;
			
				case 't':
				try{
					System.out.println("The topological sorted array is: ");
					temp = matrix.topSort();
					for(int i=0; i<temp.length; i++)
						System.out.print(temp[i] + " ");
					System.out.println();
				}catch(RuntimeException e) {
					System.out.println("Can't do topological sort - the graph is cyclic");
				}
				break;

				case 'q':
				System.out.println("Quitting!");
				break;
			
				default:
				System.out.println("Invalid choice!");
				break;
		   }
	
		}
	}
}